/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class HouseMouse extends Animal{

    @Override
    public void setMovementType() {
        this.movementType = "scurry";
    }

    @Override
    public void setMaximumAge() {
        this.maximumAge = 13;
    }

    @Override
    public void setMaximumSpeed() {
        this.maximumSpeed = 21.0;
    }

}
