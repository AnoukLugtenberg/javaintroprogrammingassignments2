/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Elephant extends Animal {

    @Override
    public void setMovementType() {
        this.movementType = "thunder";
    }

    @Override
    public void setMaximumAge() {
        this.maximumAge = 86;
    }

    @Override
    public void setMaximumSpeed() {
        this.maximumSpeed = 40.0;
    }

}
