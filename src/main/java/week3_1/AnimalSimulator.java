/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class AnimalSimulator {
    public static void main(String[] args) {
        AnimalSimulator anSim = new AnimalSimulator();
        String message = anSim.start(args);
        System.out.println(message);
    }


    public String start(String[] args) {
        //start processing command line arguments
        //TODO is only processing one line of arguments, although it counts to the arg.length
        Animal an = new Animal();
        Animal animal;
        if (args.length > 1) {
            for (int i = 0; i < args.length; i++) {
                System.out.println("args[i] = " + args[i]);
                if (args[i].equals("Horse")) {
                    animal = new Horse();
                } else if (args[i].equals("Elephant")) {
                    animal = new Elephant();
                } else if (args[i].equals("Mouse")) {
                    animal = new HouseMouse();
                } else if (args[i].equals("Tortoise")) {
                    animal = new Tortoise();
                } else if (args[i].equals("help") | args[i].equals(" ")) {
                    return getHelpMessage();

                } else {
                    return "Error: animal species " + args[i] + " is not known. run with single parameter \"help\" to get a listing of available species. Please give new values";
                }

                animal.setName(args[i]);
                animal.setAge(Integer.parseInt(args[++i]));
                animal.setMaximumSpeed();
                animal.setMovementType();
                animal.setSpeed();
                return animal.getName() + " of age " + animal.getAge() + " moving in " + animal.getMovementType() + " at " + String.format("%.1f", animal.getSpeed()).replaceAll(",", ".") + " km/h";
            }
        } else return getHelpMessage();
        return "";
    }

    
    /**
     * returns all supported animals as List, alhabetically ordered
     * @return supportedAnimals the supported animals
     */
    public List<String> getSupportedAnimals() {
        ArrayList<String> supportedAnimals = new ArrayList<>();
        supportedAnimals.add("Elephant");
        supportedAnimals.add("Horse");
        supportedAnimals.add("HouseMouse");
        supportedAnimals.add("Tortoise");

        for (String animal : supportedAnimals) {
            
        }

        return supportedAnimals;
    }

    public String getHelpMessage() {
        String helpMessage = "Usage: java AnimalSimulator <Species age Species age ...>\n" +
                "Supported species (in alphabetical order):\n";


        return helpMessage;
    }
}
