/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Animal {
    public int age;
    public String name;
    public String movementType;
    public double speed;
    public double maximumSpeed;
    public double maximumAge;


    public void setAge(int age) {
        setMaximumAge();
        if (age >= 0 && age <= this.maximumAge) {
            this.age = age;
        } else {
            System.out.println("Error: maximum age of " + this.getName() + " is " + this.getMaximumAge() + " years. Please give new values");
            System.exit(0);
        }

    }

    public void setName(String name) {
        this.name = name;
        if (this.name.startsWith("E") || this.name.startsWith("A") || this.name.startsWith("O") || this.name.startsWith("I") || this.name.startsWith("U")) {
            this.name = "An " + this.name;
        } else {
            this.name = "A " + this.name;
        }
    }

    public void setMovementType() {
        this.movementType = movementType;
    }

    public void setMaximumAge() {
        this.maximumAge = maximumAge;
    }

    public void setMaximumSpeed() {
        this.maximumSpeed = maximumSpeed;
    }

    public void setSpeed() {
        this.speed = this.maximumSpeed * (0.5 + (0.5 * ((this.maximumAge - this.age) / this.maximumAge)));
    }

    /**
     * returns the age of the animal
     * @return age of this animal
     */
    public int getAge() {
        return this.age;
    }
    
    /**
     * returns the name of the animal
     * @return name the species name
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return this.movementType;
    }
    
    /**
     * returns the speed of this animal
     * @return speed the speed of this animal
     */
    public double getSpeed() {
        return this.speed;
    }

    /**
     * returns the maximum age of this animal
     * @return the maximum age of this animal
     */
    private double getMaximumAge() {
        return this.maximumAge;
    }

    /**
     * returns the maximum speed of this animal
     * @return the maximum speed of this animal
     */
    public double getMaximumSpeed() {
        return this.maximumSpeed;
    }
    
}
