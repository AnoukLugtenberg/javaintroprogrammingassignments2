/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Horse extends Animal {

    @Override
    public void setMovementType() {
        this.movementType = "gallop";
    }

    @Override
    public void setMaximumAge() {
        this.maximumAge = 62.0;
    }

    @Override
    public void setMaximumSpeed() {
        this.maximumSpeed = 88.0;
    }

}
