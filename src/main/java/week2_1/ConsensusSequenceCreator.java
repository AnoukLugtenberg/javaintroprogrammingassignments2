/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week2_1;

import java.util.Arrays;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class ConsensusSequenceCreator {
    /**
     * testing main.
     * @param args 
     */
    public boolean iupac;

    public static void main(String[] args) {
        String[] sequences = new String[4];
        sequences[0] = "GAAT";
        sequences[1] = "GAAA";
        sequences[2] = "GATT";
        sequences[3] = "GAAC";
        
        ConsensusSequenceCreator csc = new ConsensusSequenceCreator();
        String consensus = csc.createConsensus(sequences, true);
        //consensus should equal "GAWH"
        consensus = csc.createConsensus(sequences, false);
        //consensus should equal "GA[A/T][A/T/C]"
    }

    /**
     * creates a consensus sequence from the given array of sequences.
     * @param sequences the sequences to scan for consensus
     * @param iupac flag tfo indicate IUPAC (true) or bracket notation (false)
     * @return consensus the consensus sequence
     */
    public String createConsensus(String[] sequences, boolean iupac) {
        this.iupac = iupac;

        String consensus = "";
        for (int i = 0; i < sequences[0].length(); i++) {
            String nucleotides = determineConsensusPerNucleotide(sequences, i);
            consensus += nucleotides;
        }
        return consensus;
    }

    private String determineConsensusPerNucleotide(String[] sequences, int i) {
        String nucleotidesPerIndex = determineDifferentNucleotidesPerIndex(sequences, i);
        if (nucleotidesPerIndex.length() == 1) {
            return nucleotidesPerIndex;
        } else {
            String sortedNucleotidesPerIndex = sortNucleotides(nucleotidesPerIndex);
            if (this.iupac) {
                return createPartOfConsensusIupac(sortedNucleotidesPerIndex);
            }
            else {
                return createPartOfConsensusNotIupac(sortedNucleotidesPerIndex);
            }
        }
    }
    
    private String determineDifferentNucleotidesPerIndex(String [] sequences, int i) {
        String nucleotidesPerIndex = "";
        for (String oneSequence : sequences) {
            String singleNucleotide = oneSequence.substring(i, i + 1);
            if (!nucleotidesPerIndex.contains(singleNucleotide)) {
                nucleotidesPerIndex += singleNucleotide;
            }
        }
        return nucleotidesPerIndex;
    }

    private String createPartOfConsensusIupac(String sortedNucleotidesPerIndex) {
        String iupacCode = "";
        switch (sortedNucleotidesPerIndex) {
            case "AG" : iupacCode = "R";
            break;
            case "CT" : iupacCode = "Y";
            break;
            case "CG" : iupacCode = "S";
            break;
            case "AT" : iupacCode = "W";
            break;
            case "GT" : iupacCode = "K";
            break;
            case "AC" : iupacCode = "M";
            break;
            case "CGT" : iupacCode = "B";
            break;
            case "AGT" : iupacCode = "D";
            break;
            case "ACT" : iupacCode = "H";
            break;
            case "ACG" : iupacCode = "V";
            break;
            case "ACGT" : iupacCode = "N";
            break;
            default : iupacCode = ".";
            break;
        }
        return iupacCode;
    }

    private String createPartOfConsensusNotIupac(String sortedNucleotidesPerIndex) {
        String nucs = "[";
        for (int i = 0; i < sortedNucleotidesPerIndex.length(); i++) {
            nucs += sortedNucleotidesPerIndex.substring(i, i + 1);
            nucs += "/";
        }
        nucs = nucs.substring(0, nucs.length() - 1) + "]";
        return nucs;
    }

    private String sortNucleotides(String nucleotidesPerIndex) {
        char[] charArray = nucleotidesPerIndex.toCharArray();
        Arrays.sort(charArray);
        return new String(charArray);
    }
}

