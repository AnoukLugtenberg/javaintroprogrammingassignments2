/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week1_3;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class BritishWeightUnits {
    private int pounds;
    private int ounces;
    private int grams;

    /**
     * constructs with relevant coins
     * @param pounds
     * @param ounces
     * @param grams 
     */
    public BritishWeightUnits(int pounds, int ounces, int grams) {
        this.pounds = pounds;
        this.ounces = ounces;
        this.grams = grams;
    }

    /**
     * returns the pounds
     * @return pounds
     */

    public int getPounds() {
        this.pounds = Math.round(this.grams / 454);
        this.grams = this.grams - this.pounds * 454;
        return pounds;
    }

    /**
     * returns the ounces
     * @return ounces
     */
    public int getOunces() {
        this.ounces = Math.round(this.grams / 28);
        this.grams = this.grams - this.ounces * 28;
        return ounces;
    }

    /**
     * returns the grams
     * @return grams
     */
    public int getGrams() {
        return grams;
    }

    @Override
    public String toString() {
        return "BritishUnitsCoins{" + "pounds=" + pounds + ", ounces=" + ounces + ", grams=" + grams + '}';
    }

}
