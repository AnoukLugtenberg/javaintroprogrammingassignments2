/*
 * Copyright (c) 2014 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package week1_2;

/**
 *
 * @author michiel
 */
public class AllSubstringsPrinter {
    /**
     * main method serves development purposes only.
     *
     * @param args the args
     */
    public static void main(final String[] args) {
        AllSubstringsPrinter asp = new AllSubstringsPrinter();
//        asp.printAllSubstrings("GATCG", true, true); //should print left truncated, left aligned
//        asp.printAllSubstrings("GATCG", true, false);
        asp.printAllSubstrings("GATCG", false, false);
        asp.printAllSubstrings("GATCG", false, false);
    }

    /**
     * will print all possible substrings according to arguments.
     *
     * @param stringToSubstring the string to substring
     * @param leftTruncated     flag to indicate whether the substrings should be truncated from the left (or the right)
     * @param leftAligned       flag to indicate whether the substrings should be printed left-aligned (or right-aligned)
     */
    public void printAllSubstrings(
            final String stringToSubstring,
            final boolean leftTruncated,
            final boolean leftAligned) {
        String substring = "";
        if (leftTruncated) {
            for (int i = 0; i < stringToSubstring.length(); i++) {
                substring = stringToSubstring.substring(i);
                if (leftAligned) {
                    System.out.println(substring);
                }
                if (!leftAligned) {
                    System.out.println(String.format("%" + stringToSubstring.length() + "s", substring));
                }
            }
        }
        if (!leftTruncated) {
            int x = stringToSubstring.length();
            for (int i = 0; i < stringToSubstring.length(); i++) {
                substring = stringToSubstring.substring(0, x);
                x--;
                if (leftAligned) {
                    System.out.println(substring);
                }
                if (!leftAligned) {
                    System.out.println(String.format("%" + stringToSubstring.length() + "s", substring));
                }
            }
        }
    }
}
